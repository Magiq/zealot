package main

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

func InitDB(dbUri string) *sql.DB {
	var err error

	db, err := sql.Open("mysql", dbUri)

	if err != nil {
		panic(err)
	}

	if err = db.Ping(); err != nil {
		panic(err)
	}

	return db
}
