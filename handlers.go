package main

import (
	"encoding/json"
	"log"
	"net/http"
)

type errorResponse struct {
	Message string `json:"message"`
}

func (e errorResponse) write(w http.ResponseWriter, status int) {
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(status)

	js, err := json.Marshal(e)

	if err != nil {
		log.Fatalf("Can't encode json %s", err)
	}

	js = append(js, "\n"...)
	w.Write(js)
}

func (env *Env) RegisterHandler(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)

	var u *User
	err := decoder.Decode(&u)

	if err != nil {
		Error.Println(err)
		resp := errorResponse{Message: "Bad json format"}
		resp.write(w, http.StatusInternalServerError)
		return
	}

	err = u.Validate()

	if err != nil {
		resp := errorResponse{Message: err.Error()}
		resp.write(w, http.StatusInternalServerError)
		return
	}

	isExists, err := IsUserEmailExists(env.db, u)

	if err != nil {
		resp := errorResponse{Message: "Database error"}
		resp.write(w, http.StatusInternalServerError)
		return
	}

	if isExists {
		resp := errorResponse{Message: "Such email already exists"}
		resp.write(w, http.StatusOK)
		return
	}

	_, err = CreateUser(env.db, u)

	if err != nil {
		Error.Println(err)
		resp := errorResponse{Message: "Database error"}
		resp.write(w, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
