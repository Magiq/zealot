package main

import (
	"database/sql"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"flag"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/vharitonsky/iniflags"
)

var (
	databaseURI = flag.String("databaseURI", "", "Database uri")
	port        = flag.String("port", ":8080", "Api server port")
)

type Env struct {
	db *sql.DB
}

func main() {
	InitLogger(ioutil.Discard, os.Stdout, os.Stdout, os.Stderr)
	iniflags.Parse()

	if *databaseURI == "" {
		panic("Please specify database uri")
	}

	db := InitDB(*databaseURI)
	env := &Env{db}

	r := mux.NewRouter()
	api := r.PathPrefix("/api").Subrouter()
	api.path("/register").Handler(env.RegisterHandler)

	loggedRouter := handlers.LoggingHandler(os.Stdout, r)
	log.Fatal(http.ListenAndServe(*port, loggedRouter))
}
