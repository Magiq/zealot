package main

import (
	"crypto/md5"
	"database/sql"
	"errors"

	"github.com/google/uuid"
	"encoding/hex"
)

type User struct {
	ID       int    `json:"-"`
	UUID     string `json:"-"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (u User) Validate() error {
	if u.Email == "" {
		return errors.New("Empty email")
	}

	if u.Password == "" {
		return errors.New("Empty password")
	}

	return nil
}

func CreateUser(db *sql.DB, u *User) (*User, error) {
	stmt, err := db.Prepare("INSERT INTO users SET email=?, password=?, uuid=?")

	if err != nil {
		return nil, err
	}

	passwordHashBytes := md5.Sum([]byte(u.Password))
	passwordHash := hex.EncodeToString(passwordHashBytes[:])

	u.UUID = uuid.New().String()

	result, err := stmt.Exec(u.Email, passwordHash, u.UUID)

	if err != nil {
		return nil, err
	}

	lastInsertedID, err := result.LastInsertId()

	if err != nil {
		return nil, err
	}

	u.ID = int(lastInsertedID)
	return u, nil
}

func IsUserEmailExists(db *sql.DB, u *User) (bool, error) {
	var exists bool
	err := db.QueryRow("SELECT EXISTS(SELECT 1 FROM users WHERE email=?)", u.Email).Scan(&exists)

	if err != nil {
		return false, err
	}

	return exists, nil
}
